//
//  KeyCenter.m
//  OpenVideoCall
//
//  Created by GongYuhua on 2016/9/12.
//  Copyright © 2016年 Agora. All rights reserved.
//

#import "KeyCenter.h"

@implementation KeyCenter
+ (NSString *)AppId {
    return @"346483e72d8048e7a981a16eb893ad31";
}

// assign token to nil if you have not enabled app certificate
+ (NSString *)Token {
    return nil;
}
@end
