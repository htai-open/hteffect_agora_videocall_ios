//
//  AppDelegate.m
//  OpenVideoCall
//
//  Created by GongYuhua on 2016/11/17.
//  Copyright © 2016年 Agora. All rights reserved.
//

#import "AppDelegate.h"
//todo --- HTEffect start0 ---
#import <HTEffect/HTEffectInterface.h>
//todo --- HTEffect end ---
@interface AppDelegate ()

@end

@implementation AppDelegate
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.orientation = UIInterfaceOrientationMaskPortrait;
    //todo --- HTEffect start1 ---
    # error 需要HTEffect appid，与包名绑定，请联系商务获取
    [[HTEffect shareInstance] initHTEffect:@"Your AppId" withDelegate:nil];
    //todo --- HTEffect end ---
    return YES;
}

- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window {
    return self.orientation;
}

@end
