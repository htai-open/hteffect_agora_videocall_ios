//
//  AGVideoPreProcessing.h
//  OpenVideoCall
//
//  Created by Alex Zheng on 7/28/16.
//  Copyright © 2016 Agora.io All rights reserved.
//

#import <UIKit/UIKit.h>

//todo --- HTEffect start2 ---
#import "HTUIManager.h"
#import <HTEffect/HTEffectInterface.h>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>
//todo --- HTEffect end ---

@class AgoraRtcEngineKit;

@interface AGVideoPreProcessing : NSObject

+ (void)setViewControllerDelegate:(id)viewController;
+ (int) registerVideoPreprocessing:(AgoraRtcEngineKit*) kit;
+ (int) deregisterVideoPreprocessing:(AgoraRtcEngineKit*) kit;

@end
		
